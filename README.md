UCT QuickPoll
==========

Realtime polling app for developed for UCT. Uses node.js, socket.io and jQuery/bootstrap.
Currently used across three departments at UCT, supports over 1000 concurrent users. All polls and results are stored in a mongoDB database for usage tracking.

The UCT QuickPoll webapp allows lecturers to gain rapid feedback during lectures. It is designed to be simple for students to use, and easy to set up. QuickPoll supports polls with between 2 and 8 options (labelled as A/1 through H/8). Lecturers can set up their own polls by logging in using the following URL format (appended to this page's address):


```
#!python

/lecview/{deptName}/{courseCode}
```


(e.g. PHY1004W uses the following lecturer URL: polling.uct.ac.za/lecview/phy/1004w)

Before using QuckPoll for the first time, please contact the app administrator (cmrang001@myuct.ac.za), in order to determine your departmental QuickPoll password. Students do not need to log in in order to respond to the polls. You should direct them to the relevant URL, of the form:


```
#!python

/{deptName}/{courseCode}
```


(e.g. PHY1004W uses the following student URL: polling.uct.ac.za/phy/1004w)

If multiple lecturers want to use QuckPoll simultaneously with the same course code, they should append a sub-room to both the lecturer and student URLs.

(e.g. PHY10013S uses the following student URL for sub-room 1: polling.uct.ac.za/phy/1013s/1)